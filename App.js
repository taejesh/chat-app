import React, {Component} from 'react';
import { 
  View,
  Text,
  StyleSheet
} from "react-native";
import {createStackNavigator,createDrawerNavigator,createAppContainer} from 'react-navigation';
import {HomeScreen,LoginScreen,SettingScreen} from './Screens/index';


const  AppStackNavigation = createStackNavigator ({ 

    Home :  HomeScreen,
    Login : LoginScreen, 
  Setting : SettingScreen,
  
})
const MyStackContainer=createAppContainer(AppStackNavigation);

export default class App extends Component { 
 
  render() {
        return (
        <MyDrawerContainer/>       
               );
  }
}



const  AppDrawerNavigation = createDrawerNavigator ({
  Home :  HomeScreen,
Login : LoginScreen,

Setting : SettingScreen,

})
export const MyDrawerContainer=createAppContainer(AppDrawerNavigation);

