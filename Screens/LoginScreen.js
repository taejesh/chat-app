import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import Login from '../components/login';
import Signup from '../components/signup';




class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      toggle: "ture",

    })
  }


  render() {

    return (
      <LinearGradient
        colors={['#c1432b', '#c1432b',]}
        style={styles.container}>

        <ScrollView vertical={true}>

          <View style={{ alignItems: 'center', }} >
            {/* <Image style={{ width: 50, height: 50, marginTop: 40 }} source={require('./assets/imgs/images.png')} /> */}
            <Text style={styles.logo}>
              Word Up
            </Text>
          </View>
          <View>

            <View style={styles.click}  >

              <TouchableOpacity style={styles.login} onPress={() => this.setState({ toggle: true })}>
                <Text style={this.state.toggle ? { color: 'white', fontWeight: '500', fontSize: 16 } : { color: 'white', }}>LOGIN</Text>
              </TouchableOpacity>


              <TouchableOpacity style={styles.signup} onPress={() => this.setState({ toggle: false })}>
                <Text style={this.state.toggle ? { color: 'white', } : { color: 'white', fontWeight: '500', fontSize: 16 }}>SIGN UP</Text>
              </TouchableOpacity>

            </View>

            {this.state.toggle ?
              <Login /> : <Signup />
            }
            
          </View>

        </ScrollView >
      </LinearGradient>
    )
  }
}
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    paddingTop: 10,
  },
  click: {
    flexDirection: 'row',
    paddingTop: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  aClick: {
    flexDirection: 'row',
    paddingTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 50
  },
  login: {
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderColor: 'white',
  },
  signup: {
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
