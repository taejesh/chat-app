import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
} from "react-native";
import ImagePicker from 'react-native-image-picker';
import firebase from 'react-native-firebase';


class HomeScreen extends Component {
    static navigationOptions = {
        headerTitle: 'Home',

    }
    constructor(props) {
        super(props);
        this.state = {
            uid: '',
            data: '',
            url: ''

        }
    }


    imgpicker = () => {
        let uid = this.state.uid; 
        let options = {};
        ImagePicker.launchImageLibrary(options, response => {

            const imagePath = response.uri;
           
            
            let mime = 'image/jpg';
            firebase.storage().ref(uid).child('img.jpg').put(imagePath, { contentType: mime })
                .then((snapshot) => {
                    return snapshot.downloadURL;
                }).then((url) => {
                    firebase.database().ref('users/' + uid).update({
                        imgURL:url
                    });
                })


            // imageRef.on('state_changed',(snapshot)=>{
            //     imageRef.snapshot.getDownloadURL().then((downloadURL) =>{
            //         console.log('File available at', downloadURL);
            //       });
            //    this.setState({url:snapshot.downloadURL})
            // });


        })



    }
    componentDidMount() {
      let  uid= firebase.auth().currentUser.uid
            this.setState({
               uid
            });
     
        firebase.database().ref('users/'+ uid).on('value',(snapshot)=> {
        console.log(snapshot.val())
        }
        )
    }


    render() {
        const url = this.state.url
        return (
            <View style={styles.container}>

                <TouchableOpacity onPress={this.user}><Text>toggle</Text></TouchableOpacity>
                <TouchableOpacity  >
                    <Text style={{ color: '#000', textAlign: 'center' }} onPress={this.imgpicker} >UPLOAD Image</Text>
                </TouchableOpacity>
                <Image
                 
                />
            </View>
        );
    }
}
export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});