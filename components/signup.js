import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from "react-native";
import firebase from 'react-native-firebase';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            email: '',
            password: '',
            firstname: '',
            secondname: '',
            img: ''

        })
    }


    //     imgpicker=()=>{
    //         const options={};
    //   ImagePicker.launchImageLibrary(options,response=>{
    //     console.log( response)
    //   })
    // }

    register = (email, password) => {
        const { firstname, secondname } = this.state;

        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((user) => {
                const uid = user.user.uid;
                firebase.database().ref('users/' + uid).set({
                    uid: uid,
                    username: firstname + " " + secondname,
                    email: email,
                });

                this.setState({ email: '', password: '', firstname: '', secondname: '' })
            });


    }




    render() {
        return (
            <View style={styles.container}>
                <View style={{ paddingTop: 40, alignItems: 'center' }}>

                    <TextInput
                        style={styles.input}
                        placeholder="First Name"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        keyboardType="text"
                        onChangeText={(firstname) => this.setState({ firstname })} value={this.state.firstname}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder="Second Name"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        keyboardType="text"
                        onChangeText={(secondname) => this.setState({ secondname })} value={this.state.secondname}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        onChangeText={(email) => this.setState({ email })} value={this.state.email}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({ password })} value={this.state.password}
                    />
                    <TouchableOpacity style={styles.btnimg} >
                        <Text style={{ color: '#000', textAlign: 'center' }} onPress={this.imgpicker} >UPLOAD Image</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.loginB} >
                        <Text style={{ color: '#000', textAlign: 'center' }} onPress={() => this.register(this.state.email, this.state.password)}>SIGN UP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default Signup;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 250,
        borderBottomWidth: 1,
        borderColor: 'white'
    },
    loginB: {
        width: 200,
        padding: 10,
        margin: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        backgroundColor: "white"

    },
    btnimg: {
        flex: 1,
        marginTop: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,

    },
});