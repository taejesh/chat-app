import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from "react-native";
import firebase from 'react-native-firebase';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            email: '',
            password: '',

        })
    }

    signin = (email, password) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                this.props.navigation.navigate('Home')

               
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ paddingTop: 50, alignItems: 'center' }}>
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        onChangeText={(email) => this.setState({ email })} value={this.state.email}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        placeholderTextColor="white"
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({ password })} value={this.state.password}
                    />
                    <TouchableOpacity style={styles.loginB}>
                        <Text style={{ color: '#000', textAlign: 'center' }} onPress={() => this.signin(this.state.email, this.state.password)}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 250,
        borderBottomWidth: 1,
        borderColor: 'white'
    },
    loginB: {
        width: 200,
        padding: 10,
        margin: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        backgroundColor: "white"

    },
});